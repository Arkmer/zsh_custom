# Python was installed with the standard install from Python.org

# What do you want to do with Python commands?
alias py=python3
alias pip=pip3
alias pi="pip3 install"
alias pu="pip3 uninstall -y"
alias pipv="virtualenv ."  # Create a virtual environment in the current directory
alias pipva="source bin/activate" # Activate the current virtual environment
alias pipvd="deactivate"  # Deactivate the current virtual environment

# Python functions
pyenv() { # Create a new virtual environment, install all packages
  local desc=$1
  pipv
  pipva
  pi "$desc"
}

# Global Environment Mass Python Package Remover
gempypr() {
  local packages_to_keep=("pip" "setuptools" "virtualenv" "platformdirs" "distlib" "filelock")  # Array containing the packages to keep

  # Get a list of installed Python packages
  local installed_packages=($(pip3 list --format=columns | awk 'NR>2 {print $1}'))

  # Loop over each installed package
  for package in "${installed_packages[@]}"; do
    # Check if the package is in the list of packages to keep
    if [[ ! " ${packages_to_keep[@]} " =~ " ${package} " ]]; then
      # Uninstall the package
      echo "Uninstalling $package..."
      pip3 uninstall -y "$package"
    fi
  done
  pip list
}
newpy() {
  pipv
  pipva
  rm .gitignore
  cp /Users/ryanudelhoven/.oh-my-zsh/custom/standard_files/python/.gitignore .gitignore
  clear
}

# Needs a dependency check (pip3 check) built into it.
# Probably messy uninstalling then reinstalling things.
# Could output the new list of installed packages at the end.
# Then rewrite the hard coded list of packages to keep?
# That should avoid future issues...
# It'll make a mess of other environments though.
