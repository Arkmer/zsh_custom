# Aliases
alias ..="cd .."
alias .2="cd ../.."
alias ll="ls -a"
alias cdev="cd ~/development"
alias cdo="cd ~/development/docker"
alias cdz="cd ~/.oh-my-zsh"
# alias pcat='pygmentize -f terminal256 -O style=native -g'
    # Just use "cat text.js" to read inside files.
    # Need to learn about pygmentize, no idea what I just installed.
    # https://pygments.org/docs/quickstart/

# Functions
code(){
    open -a "Visual Studio Code" $1
};
mkcd(){
    mkdir $1
    cd $1
};
mkcd.(){
    mkdir $1
    cd $1
    code .
};
.zsh(){
    code ~/.oh-my-zsh/
};
cd.(){
    cd $1
    code .
};