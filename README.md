# Oh-My-zsh Commands!
## Why?
Most things are just aliases that serve obviously general purposes.

Some things are commands that clean up messes.
- Most of the docker commands clear everything so I can test from scratch
- Some of the python commands are for keeping environments clean

## Broken Things
I did leave a few broken things so I could come back to them later.
