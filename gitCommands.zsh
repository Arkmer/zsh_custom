# Aliases
alias gcm="git checkout master";
alias gcl="git checkout -";
alias gcb="git checkout -b"

# Functions
commit:(){ # commit: [description]
    local desc=$1
    shift
    while [[ $# -gt 0 ]] do
        desc+=' '
        desc+=${1}
        shift
    done
    git add .
    git status
    git commit -m "$desc"
};
sz(){ # Resets your zsh terminal
    source ~/.zshrc
    clear
}
mm(){ # merge branch
    gcm
    git pull
    gcl
    git merge $(branch_parser)
};
gpm(){ # gpm [-y, -n, -nb] [branch] => git pull master
    if [[ $1 == "-y" ]] then
        pullMaster
        git checkout -
        git merge $(branch_parser)
        clear
    elif [[ $1 == "-n" ]] then
        pullMaster
        clear
    elif [[ $1 == "-nb" ]] then
        pullMaster
        gcb "$2"
    else
        echo 'Inputs not recognized'
        echo 'Use -y to return to your branch'
        echo 'Use -n to stay in current branch'
        echo 'Use -nb to start a new branch'
    fi
}
pullMaster(){ # Not used by itself
    gcm
    git pull
}
push(){ # push [-f, --loud] => git push
    if [[ $# == 0 ]] then
        commit_called
        git push
        clear
    elif [[ $1 == '-f' ]] then
        commit_called
        git push --set-upstream origin $(branch_parser)
        clear
    elif [[ $1 == '--loud' ]] then
        commit_called
        git push
    elif [[ $1 == '-h' ]] then
        echo 'Inputs:'
        echo '  Nothing, -f, --loud'
    fi
}
branch_parser(){ # Not used by itself
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}
commit_called(){ # Not used by itself
    read '?Message: ' desc
    if [[ $desc == '' ]] then
        desc='Nothing note worthy.'
    fi
    git add .
    git status
    git commit -m "$desc"
}
gdb(){ # gdb [-l, -r, -b] [branch] => git delete branch
    if [[ $1 == '-l' ]] then # Local
        git branch -D "$2"
    elif [[ $1 == '-r' ]] then # Repo
        git push origin --delete "$2"
    elif [[ $1 == '-b' ]] then # Both
        gdb -l "$2"
        gdb -r "$2"
    fi
}
.zsh_save(){
    rm -rf /Users/ryanudelhoven/development/.zsh_custom_files/*
    cp -a ~/.oh-my-zsh/custom/. /Users/ryanudelhoven/development/.zsh_custom_files/
    cd /Users/ryanudelhoven/development/.zsh_custom_files
    commit_called
    git push
    cdz
    sz
    clear
}

# fix_branch() {
#     git checkout -b "$1"
#     commit: branch fix
#     git push --set-upstream origin $(branch_parser)
#     gcm
#     git reset --hard origin/master
# } # For some reason this doesn't roll back the code typed into the env.
# test